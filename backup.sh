#!/bin/bash

DIR=$(realpath $(dirname "$0"))
cd ${DIR}
. venv/bin/activate
FILENAME=$(date +"%Y-%m-%d.json")
[ ! -d backup ] && mkdir backup
./manage.py dumpdata > backup/"${FILENAME}"
