from rest_framework import routers
from api import views

router = routers.DefaultRouter()

router.register(r'bibtextype', views.BibtexTypeView, 'bibtextype')

router.register(r'type', views.TypeView, 'type')

router.register(r'label', views.LabelView, 'label')

router.register(r'collaboration', views.CollaborationView, 'collaboration')

router.register(r'author', views.AuthorView, 'author')

router.register(r'author_post', views.AuthorRawView, 'author_post')

router.register(r'conference', views.ConferenceView, 'conference')

router.register(r'journal', views.JournalView, 'journal')

router.register(r'publication', views.PublicationView, 'publication')

router.register(r'pub_post', views.PublicationRawView, 'pub_post')
