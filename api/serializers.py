
from rest_framework.serializers import ModelSerializer
from .models import BibtexType, Type, Label, Collaboration, Author, Conference, Journal, Publication


class BibtexTypeSerializer(ModelSerializer):
    class Meta:
        model = BibtexType
        fields = ['name', 'id', 'template']


class TypeSerializer(ModelSerializer):
    bibtex_type = BibtexTypeSerializer(read_only=False, many=False)

    class Meta:
        model = Type
        fields = ['name', 'id', 'bibtex_type']


class LabelSerializer(ModelSerializer):
    class Meta:
        model = Label
        fields = ['id', 'label']


class CollaborationSerializer(ModelSerializer):
    class Meta:
        model = Collaboration
        fields = ['id', 'name', 'inspire']


class AuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = ['id', 'name', 'firstname', 'inspire', 'orcid']


class ConferenceSerializer(ModelSerializer):
    class Meta:
        model = Conference
        fields = ['id', 'brief', 'name', 'name_german', 'location', 'url', 'start', 'end', 'inspire']


class JournalSerializer(ModelSerializer):
    class Meta:
        model = Journal
        fields = ['id', 'brief', 'name', 'abbreviation', 'inspire']


class PublicationSerializer(ModelSerializer):
    type = TypeSerializer(read_only=False, many=False)
    label = LabelSerializer(read_only=False, many=True)
    author = AuthorSerializer(read_only=False, many=True)
    presenter = AuthorSerializer(read_only=False, many=False)
    collaboration = CollaborationSerializer(read_only=False, many=False)
    conference = ConferenceSerializer(read_only=False, many=False)
    journal = JournalSerializer(read_only=False, many=False)

    class Meta:
        model = Publication
        fields = ['id', 'title', 'title_text', 'title_german', 'title_german_text', 'author', 'author_list', 'presenter',
                  'collaboration', 'type', 'label', 'date', 'url', 'journal', 'volume', 'number', 'pages',
                  'doi', 'arxiv', 'inspire', 'cite', 'bibtex', 'conference', 'file', 'comment']


class PublicationRawSerializer(ModelSerializer):
    """Serializer for publications where relations are represented by primary key indices"""

    class Meta:
        model = Publication
        fields = ['id', 'title', 'title_text', 'title_german', 'title_german_text', 'author', 'author_list', 'presenter',
                  'collaboration', 'type', 'label', 'date', 'url', 'journal', 'volume', 'number', 'pages',
                  'doi', 'arxiv', 'inspire', 'cite', 'bibtex', 'conference', 'file', 'comment']
        depth = 0
