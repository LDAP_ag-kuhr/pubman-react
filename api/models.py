from django.db import models


class BibtexType(models.Model):
    name = models.CharField(max_length=50, unique=True)
    template = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Type(models.Model):
    name = models.CharField(max_length=200, unique=True)
    bibtex_type = models.ForeignKey(BibtexType, on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Label(models.Model):
    label = models.CharField(max_length=200, unique=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['label']

    def __str__(self):
        return self.label


class Collaboration(models.Model):
    name = models.CharField(max_length=200, unique=True)
    inspire = models.CharField(max_length=40, blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Author(models.Model):
    name = models.CharField(max_length=40)
    firstname = models.CharField(max_length=40)
    inspire = models.CharField(max_length=40, blank=True)
    orcid = models.CharField(max_length=40, blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Conference(models.Model):
    brief = models.CharField(max_length=40, unique=True)
    name = models.CharField(max_length=200)
    name_german = models.CharField(max_length=200, blank=True)
    location = models.CharField(max_length=200, blank=True)
    url = models.URLField(null=True, blank=True, max_length=200)
    start = models.DateField(blank=True)
    end = models.DateField(blank=True)
    inspire = models.CharField(max_length=40, blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-start', 'brief']

    def __str__(self):
        return self.brief


class Journal(models.Model):
    name = models.CharField(max_length=200)
    brief = models.CharField(max_length=40, unique=True)
    abbreviation = models.CharField(max_length=10, blank=True)
    inspire = models.CharField(max_length=40, blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['brief']

    def __str__(self):
        return self.brief


def upload_path(instance, filename):
    type = instance.type.name.split()[0]
    path = f'{type.lower()}/{instance.date.year}'
    if (instance.type.name.endswith('Thesis')):
        path = f'{path}/{type}{instance.author.all()[0].firstname}{instance.author.all()[0].name}.pdf'
    elif (type in ['Talk', 'Poster', 'Proceedings']):
        path = f'{path}/{instance.conference.brief}_{instance.author.all()[0].firstname}{instance.author.all()[0].name}_{instance.date}.pdf'
    elif (instance.arxiv):
        path = f'{path}/{instance.arxiv}.pdf'
    else:
        path = f'{path}/{filename}'
    if (instance.file):
        instance.file.delete()
    return path


class Publication(models.Model):
    id = models.BigAutoField(primary_key=True)
    type = models.ForeignKey(Type, on_delete=models.PROTECT, null=False, default=1)
    label = models.ManyToManyField(Label, blank=True)
    title = models.CharField(max_length=400)
    title_text = models.CharField(max_length=400, blank=True)
    title_german = models.CharField(max_length=400, blank=True)
    title_german_text = models.CharField(max_length=400, blank=True)
    date = models.DateField(blank=False, null=False)
    collaboration = models.ForeignKey(Collaboration, on_delete=models.PROTECT, null=True, blank=True)
    author = models.ManyToManyField(Author, blank=False)
    author_list = models.CharField(max_length=400, blank=True)
    presenter = models.ForeignKey(Author, related_name='presentation', on_delete=models.PROTECT, null=True, blank=True)
    conference = models.ForeignKey(Conference, on_delete=models.PROTECT, null=True, blank=True)
    journal = models.ForeignKey(Journal, on_delete=models.PROTECT, null=True, blank=True)
    volume = models.CharField(max_length=40, blank=True)
    number = models.CharField(max_length=40, blank=True)
    pages = models.CharField(max_length=40, blank=True)
    doi = models.CharField(max_length=100, blank=True)
    arxiv = models.CharField(max_length=40, blank=True)
    inspire = models.CharField(max_length=40, blank=True)
    cite = models.CharField(max_length=40, blank=True)
    bibtex = models.TextField(blank=True)
    url = models.URLField(null=True, blank=True, max_length=200)
    file = models.FileField(upload_to=upload_path, blank=True, null=True)
    comment = models.CharField(max_length=400, blank=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-date', 'title', 'type', 'created']

    def __str__(self):
        return self.title
