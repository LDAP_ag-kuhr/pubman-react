from .models import BibtexType, Type, Label, Collaboration, Author, Conference, Journal, Publication
from .serializers import BibtexTypeSerializer, TypeSerializer, LabelSerializer, CollaborationSerializer, AuthorSerializer, ConferenceSerializer, JournalSerializer, PublicationSerializer, PublicationRawSerializer
from rest_framework.response import Response
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend
from django.middleware.csrf import get_token
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.decorators.http import require_POST
import json


######################################
########### Publication API ##########
######################################

class BibtexTypeView(viewsets.ReadOnlyModelViewSet):
    queryset = BibtexType.objects.all()
    serializer_class = BibtexTypeSerializer


class TypeView(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Type.objects.all()
    serializer_class = TypeSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['name']
    ordering_fields = ['name']


class LabelView(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Label.objects.all()
    serializer_class = LabelSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['label']
    ordering_fields = ['label', 'updated']
    ordering = ['label']


class CollaborationView(viewsets.ReadOnlyModelViewSet):
    queryset = Collaboration.objects.all()
    serializer_class = CollaborationSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['name', 'inspire']
    ordering = ['name']


class AuthorView(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['name', 'firstname', 'inspire', 'orcid']
    ordering_fields = ['name', 'firstname']
    ordering = ['name', 'firstname']


class AuthorRawView(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['name', 'firstname', 'inspire', 'orcid']
    ordering_fields = ['name', 'firstname']
    ordering = ['name', 'firstname']


class ConferenceView(viewsets.ModelViewSet):
    queryset = Conference.objects.all()
    serializer_class = ConferenceSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['brief', 'inspire']
    ordering_fields = ['start', 'brief']
    ordering = ['-start', 'brief']


class JournalView(viewsets.ReadOnlyModelViewSet):
    queryset = Journal.objects.all()
    serializer_class = JournalSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    search_fields = ['brief', 'inspire']
    ordering_fields = ['brief']
    ordering = ['brief']


class PublicationView(viewsets.ReadOnlyModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Publication.objects.prefetch_related('label', 'author', 'collaboration', 'conference', 'journal').select_related('type')
    serializer_class = PublicationSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_fields = {'type': ['exact'], 'type__id': ['in'], 'label': ['exact'], 'label__id': ['in'], 'author': ['exact'], 'author__id': ['in'], 'date': ['gte', 'lte', 'range']}
    search_fields = ['title', 'title_text', 'title_german', 'title_german_text', 'author__name', 'author__firstname', 'collaboration__name', 'label__label', 'conference__brief', 'conference__name', 'conference__name_german']
    ordering = ['-date', 'title', 'type']


class PublicationRawView(viewsets.ModelViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationRawSerializer
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filterset_fields = {'type': ['exact'], 'type__id': ['in'], 'label': ['exact'], 'label__id': ['in'], 'author': ['exact'], 'author__id': ['in'], 'date': ['gte', 'lte', 'range']}
    search_fields = ['title', 'title_text', 'title_german', 'title_german_text', 'author__name', 'author__firstname', 'collaboration__name', 'label__label', 'conference__brief', 'conference__name', 'conference__name_german']
    ordering = ['-date', 'title', 'type']


class FileUploadView(APIView):
    parser_classes = [FileUploadParser]

    def put(self, request, id, filename, format=None):
        file = request.data['file']
        if id == 'undefined':
            publication = Publication.objects.latest('pk')
        else:
            publication = Publication.objects.get(pk=id)
        if (publication):
            publication.file.save(file.name, file, save=True)
        return Response({}, status=201)


@require_POST
def login_view(request):
    data = json.loads(request.body)
    username = data.get('username')
    password = data.get('password')

    if username is None or password is None:
        return JsonResponse({'detail': 'Please provide username and password.'}, status=401)

    user = authenticate(username=username, password=password)

    if user is None:
        return JsonResponse({'detail': 'Invalid credentials.'}, status=401)

    login(request, user)
    return JsonResponse({'detail': 'Successfully logged in.'})


def logout_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({'detail': 'You\'re not logged in.'}, status=401)

    logout(request)
    return JsonResponse({'detail': 'Successfully logged out.'})


@ensure_csrf_cookie
def session_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({'isAuthenticated': False}, status=401)

    return JsonResponse({'isAuthenticated': True})


def whoami_view(request):
    if not request.user.is_authenticated:
        return JsonResponse({'isAuthenticated': False}, status=401)

    return JsonResponse({'username': request.user.username})


def csrf(request):
    return JsonResponse({'csrfToken': get_token(request)})
