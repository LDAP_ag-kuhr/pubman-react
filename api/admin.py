from django.contrib import admin

# Register your models here.
from .models import BibtexType, Type, Label, Collaboration, Author, Conference, Journal, Publication

admin.site.register(BibtexType)
admin.site.register(Type)
admin.site.register(Label)
admin.site.register(Collaboration)
admin.site.register(Author)
admin.site.register(Conference)
admin.site.register(Journal)
admin.site.register(Publication)
