from django.contrib import admin
from django.urls import path, include, re_path
from api import routers
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage
from api import views
from rest_framework.documentation import include_docs_urls

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/v1/upload/(?P<id>[^/]+)/(?P<filename>[^/]+)$', views.FileUploadView.as_view()),
    path('api/v1/', include(routers.router.urls)),
    path('api-auth/', include('rest_framework.urls')),
    path('login/', views.login_view, name='api-login'),
    path('logout/', views.logout_view, name='api-logout'),
    path('session/', views.session_view, name='api-session'),
    path('whoami/', views.whoami_view, name='api-whoami'),
    path('csrf/', views.csrf, name='csrf'),
    path('api/docs/', include_docs_urls(title="Pubman API Documentation")),
]

for file in ['favicon.ico', 'manifest.json', 'robots.txt']:
    urlpatterns.append(path(file, RedirectView.as_view(url=staticfiles_storage.url(file))))

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    re_path(r"^$", TemplateView.as_view(template_name='index.html')),
    re_path(r"^(?:.*)/?$", TemplateView.as_view(template_name='index.html')),
]
