#!/bin/bash

DIR=$(realpath $(dirname "$0"))
cd ${DIR}

if [ -f pubman.pid ]; then
  kill $(cat pubman.pid)
  sleep 3
  rm pubman.pid
fi

. venv/bin/activate
nohup ./manage.py runserver 0.0.0.0:8000 &> pubman.log &
PID=$!
echo ${PID} > pubman.pid
