# pull official base image
FROM python:3

# set environment variables
ENV PYTHONUNBUFFERED 1

# install dependencies
COPY ./requirements.txt .
RUN pip install -r requirements.txt
RUN pip install legacy-cgi

# set work directory
WORKDIR /pubman

EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
