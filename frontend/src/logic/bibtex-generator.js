export function generateBibtex(publication) {
	let type;
	let cite;
	const attributes = {};

	// get information from existing bibtex
	if (publication.bibtex) {
		const match = publication.bibtex.match(/@(\w*){\s*(.*)\s*,/);
		if (match) {
			type = match[1];
			cite = match[2];
		}
		Array.from(publication.bibtex.matchAll(/\s*(.*)=\s*(.*)/g))?.forEach((match) => {
			if (match[1]) {
				attributes[match[1].trim()] = match[2].trim().replace(/^{|},$|,$|}$/g, "");
			}
		});
	}

	// get information from publication
	if (publication.type) {
		type = publication.type.bibtex_type.name;
	}
	if (publication.cite) {
		cite = publication.cite;
	}
	const fields = { ...publication };
	if (!publication.author_list) {
		let authorList = [];
		publication.author?.forEach((author) => {
			authorList.push(`${author.name}, ${author.firstname[0]}.`);
		});
		if (authorList.length > 20) {
			authorList = authorList.slice(0, 19) + ["others"];
		}
		fields.authors = authorList.join(" and ");
	} else {
		fields.authors = publication.author_list;
	}
	fields.year = publication?.date.year;
	fields.month = publication?.date.month;
	fields.day = publication?.date.day;
	fields.collaboration = publication?.collaboration?.name;
	Array.from(publication.type.bibtex_type.template.matchAll(/(\w*)=(.*)/g))?.forEach((match) => {
		let value = match[2];
		Object.entries(fields).forEach((entry) => {
			if (entry[1]) {
				value = value.replace(`{${entry[0]}}`, entry[1]);
			}
		});
		if (value[0] !== "{") {
			attributes[match[1]] = value;
		}
	});

	// construct bibtex
	if (!type) return "";
	if (!cite) {
		cite = `${publication.author[0].name}.${publication.date}`;
	}
	let bibtex = `@${type}{${cite}`;
	Object.entries(attributes).forEach((entry) => {
		bibtex += `,\n\t${entry[0]} = {${entry[1]}}`;
	});
	bibtex += "\n}";
	return bibtex;
}
export default generateBibtex;
