import { useContext, createContext } from "react";

export const defaultAppContext = {
	isLoggedIn: false,
	setIsLoggedIn: () => {}
};

export const AppContext = createContext(defaultAppContext);
export const useAppContext = () => useContext(AppContext);
