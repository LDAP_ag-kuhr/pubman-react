import { fetchBackend } from "./fetch-options";

export async function checkIsLoggedIn() {
	const response = await fetchBackend("/whoami/");
	return response.status === 200;
}
