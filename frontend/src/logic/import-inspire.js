import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

export function importAuthor(author, setAuthor) {
	let query = null;
	if (author.inspire) {
		query = `/${author.inspire}`;
	} else if (author.orcid) {
		query = `?q=${author.orcid}`;
	} else if (author.name && author.firstname) {
		query = `?q=${author.name},${author.firstname}`;
	}
	if (!query) {
		toast.error("Fill out the Inspire ID, ORCID, or Name and First Name fields", { theme: "colored" });
		return;
	}
	const result = { ...author };
	fetch(`https://inspirehep.net/api/authors${query}`)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			const metadata = "hits" in data ? data.hits.hits[0].metadata : data.metadata;
			if ("name" in metadata) {
				if (!result.name) {
					result.name = metadata.name.value.split(",")[0].trim();
				}
				if (!result.firstname) {
					result.firstname = metadata.name.value.split(",")[1].trim();
				}
			}
			if ("ids" in metadata && !result.orcid) {
				result.orcid = metadata.ids.find((entry) => entry.schema === "ORCID")?.value || "";
			}
			if (!result.inspire) {
				result.inspire = "hits" in data ? data.hits.hits[0].id : data.id;
			}
			setAuthor(result);
		});
}

export function importConference(conference, setConference) {
	let query = null;
	if (conference.inspire) {
		query = `/${conference.inspire}`;
	} else if (conference.brief) {
		query = `?q=${conference.brief}`;
	}
	if (!query) {
		toast.error("Fill out the Inspire ID or Brief Name", { theme: "colored" });
		return;
	}
	const result = { ...conference };
	fetch(`https://inspirehep.net/api/conferences${query}`)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			const metadata = "hits" in data ? data.hits.hits[0].metadata : data.metadata;
			if ("acronyms" in metadata && !result.brief) {
				result.brief = metadata.acronyms[0];
			}
			if ("titles" in metadata && !result.name) {
				result.name = metadata.titles[0].title;
			}
			if ("urls" in metadata && !result.url) {
				result.url = metadata.urls[0].value;
			}
			if ("addresses" in metadata && !result.location) {
				result.location = metadata.addresses[0]?.cities[0] || "";
			}
			if ("opening_date" in metadata && !result.start) {
				result.start = metadata.opening_date;
			}
			if ("closing_date" in metadata && !result.end) {
				result.end = metadata.closing_date;
			}
			if (!result.inspire) {
				result.inspire = "hits" in data ? data.hits.hits[0].id : data.id;
			}
			setConference(result);
		});
}

export function importPublication(publication, setPublication, journals, collaborations, authors) {
	let query = null;
	if (publication.inspire) {
		query = `recid%3A${publication.inspire}`;
	} else if (publication.arxiv) {
		if (publication.arxiv.includes(".")) {
			query = `arxiv%3A${publication.arxiv}`;
		} else if (publication.arxiv.includes("/")) {
			query = `eprint%3A${publication.arxiv}`;
		}
	}
	if (!query) {
		toast.error("Fill out the Inspire ID or arXiv ID", { theme: "colored" });
		return;
	}
	const result = { ...publication };
	fetch(`https://inspirehep.net/api/literature?q=${query}&format=json`)
		.then((response) => {
			return response.json();
		})
		.then((data) => {
			const metadata = data.hits.hits[0].metadata;
			if ("titles" in metadata && !result.title) {
				result.title = metadata.titles[0].title;
				if (metadata.titles.length > 1) {
					const alternativeTitle = metadata.titles[1].title;
					if (result.title.includes("$") && !alternativeTitle.includes("$")) {
						result.title_text = alternativeTitle;
					} else if (!result.title.includes("$") && alternativeTitle.includes("$")) {
						result.title_text = result.title;
						result.title = alternativeTitle;
					}
				}
			}
			if (!result.date) {
				if ("imprints" in metadata && !Number.isNaN(new Date(metadata.imprints[0].date))) {
					result.date = new Date(metadata.imprints[0].date).toISOString().substring(0, 10);
				} else if ("earliest_date" in metadata && !Number.isNaN(new Date(metadata.earliest_date))) {
					result.date = new Date(metadata.earliest_date).toISOString().substring(0, 10);
				}
			}
			if ("collaborations" in metadata && metadata.collaborations[0]?.record?.$ref && !result.collaboration) {
				const id = metadata.collaborations[0]?.record?.$ref.split("/").pop();
				result.collaboration = collaborations.find(
					(x) => x.name === metadata.collaborations[0].value || x.inspire === id
				);
			}
			if ("authors" in metadata) {
				metadata.authors.forEach((entry) => {
					const id = entry.record.$ref.split("/").pop();
					const author = authors.find((x) => x.inspire === id);
					if (author) {
						if (!result.author) {
							result.author = [author];
						} else if (!result.author.find((x) => x.inspire === id)) {
							result.author.push(author);
						}
					}
				});
			}
			if ("authors" in metadata && !result.author_list) {
				let authorList = [];
				metadata.authors.forEach((entry) => {
					authorList.push(entry.full_name);
				});
				if (authorList.length > 10) {
					authorList = authorList.slice(0, authorList.length > 20 ? 1 : 5);
					authorList.push("others");
				}
				result.author_list = authorList.join(" and ");
			}
			if ("publication_info" in metadata) {
				const pubInfo = metadata.publication_info[0];
				if (!result.journal) {
					let journal = null;
					if ("journal_record" in pubInfo) {
						const id = pubInfo.journal_record.$ref.split("/").pop();
						journal = journals.find((x) => x.inspire === id);
					} else if ("journal_title" in pubInfo) {
						journal = journals.find((x) => x.brief === pubInfo.journal_title);
					}
					if (!journal && "journal_title" in pubInfo) {
						let journalList = [pubInfo.journal_title];
						if ("journal_title_variants" in metadata) {
							journalList = journalList.concat(metadata.journal_title_variants);
						}
						journal = journals.find((x) => journalList.includes(x.brief));
					}
					if (journal) {
						result.journal = journal;
					}
					result.volume = pubInfo?.journal_volume;
					result.number = pubInfo?.artid;
					result.pages = pubInfo?.page_start;
				}
			}
			if ("dois" in metadata && !result.doi) {
				result.doi = metadata.dois[0].value;
			}
			if ("arxiv_eprints" in metadata && !result.arxiv) {
				result.arxiv = metadata.arxiv_eprints[0].value;
			}
			if ("id" in data.hits.hits[0] && !result.inspire) {
				result.inspire = data.hits.hits[0].id;
			}
			return fetch(`https://inspirehep.net/api/literature?q=${query}&format=bibtex`);
		})
		.then((response) => {
			return response.text();
		})
		.then((data) => {
			if (!result.bibtex) {
				result.bibtex = data;
				const match = data.match(/@\w*{\s*(.*)\s*,/);
				if (match) result.cite = match[1];
			}
			setPublication(result);
		});
}
