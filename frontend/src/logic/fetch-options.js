let csrfToken = null;

export const fetchBackend = async (url, method, body, upload = null) => {
	if (!csrfToken) {
		// eslint-disable-next-line no-async-promise-executor
		csrfToken = new Promise(async (resolve) => {
			const res = await fetch(`${window.location.origin}/csrf/`, {
				credentials: "include"
			});
			const data = await res.json();
			resolve(data.csrfToken);
		});
	}
	const getBody = () => {
		if (!body) {
			return body;
		}
		if (typeof body === "object") {
			return JSON.stringify(body);
		}
		return body;
	};

	const data = {
		headers: {
			"X-CSRFToken": await csrfToken,
			"Content-Type": "application/json; charset=UTF-8"
		},
		credentials: "include",
		body: getBody(),
		method
	};
	if (upload) {
		data.headers["Content-Type"] = upload.type;
		data.headers["Content-Length"] = `${upload.size}`;
		data.body = upload;
	}
	const response = await fetch(window.location.origin + url, data);
	if (response.status !== 500 && response.status !== 204 && response.status !== 403) {
		return response.json().then((data) => ({
			data,
			status: response.status
		}));
	}
	if (response.status === 403) {
		window.location.reload();
	}
	return response.status;
};
