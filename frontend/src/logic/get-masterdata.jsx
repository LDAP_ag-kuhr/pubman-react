import { useEffect, useState } from "react";
import { fetchBackend } from "./fetch-options";

export const getMasterData = () => {
	const [types, setTypes] = useState([]);
	const [labels, setLabels] = useState([]);
	const [collaborations, setCollaborations] = useState([]);
	const [authors, setAuthors] = useState([]);
	const [journals, setJournals] = useState([]);
	const [conferences, setConferences] = useState([]);

	const getTypes = async () => {
		const response = await fetchBackend("/api/v1/type/");
		const data = response.data;
		setTypes(data);
	};

	const getLabels = async () => {
		const response = await fetchBackend("/api/v1/label/");
		const data = response.data;
		setLabels(data);
	};

	const getCollaborations = async () => {
		const response = await fetchBackend("/api/v1/collaboration/");
		const data = response.data;
		setCollaborations(data);
	};

	const getAuthors = async () => {
		const response = await fetchBackend("/api/v1/author/");
		const data = response.data;
		setAuthors(data);
	};

	const getConferences = async () => {
		const response = await fetchBackend("/api/v1/conference/");
		const data = response.data;
		setConferences(data);
	};

	const getJournals = async () => {
		const response = await fetchBackend("/api/v1/journal/");
		const data = response.data;
		setJournals(data);
	};

	const loadData = () => {
		getTypes();
		getLabels();
		getAuthors();
		getCollaborations();
		getConferences();
		getJournals();
	};

	useEffect(() => {
		loadData();
	}, []);

	return { types, labels, collaborations, authors, conferences, journals, refetch: loadData };
};
