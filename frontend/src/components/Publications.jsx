import React, { useState, useEffect, useId, useRef } from "react";
import { Link, useNavigate } from "react-router-dom";
import CustomSelect from "./CustomSelect";
import { fetchBackend } from "../logic/fetch-options.js";

import { getMasterData } from "../logic/get-masterdata";

function Publications() {
	const [publications, setPublications] = useState([]);
	const { types, labels, authors } = getMasterData();
	const [count, setCount] = useState([]);
	const [limit, setLimit] = useState(1000);
	const [search, setSearch] = useState({
		searchInput: "",
		type: "",
		label: "",
		author: "",
		start: null,
		end: null,
		date: "",
		order: ""
	});
	const timer = useRef(null);
	const navigate = useNavigate();

	const searchItems = async () => {
		const response = await fetchBackend(
			`/api/v1/publication/?search=${search.searchInput}${search.type}${search.label}${search.author}${search.date}${search.order}&limit=${limit}`
		);
		if (response?.data) {
			const data = response.data;
			setCount(data.count);
			setPublications(data.results);
		} else {
			setCount(0);
			setPublications([]);
		}
	};

	const valSearch = () => {
		searchItems();
	};

	const setSearchType = (value) => {
		if (value) {
			setSearch({ ...search, type: `&type__id__in=${value.map((x) => x.value).join(",")}` });
		} else {
			setSearch({ ...search, type: "" });
		}
	};

	const setSearchLabel = (value) => {
		if (value) {
			setSearch({ ...search, label: `&label__id__in=${value.map((x) => x.value).join(",")}` });
		} else {
			setSearch({ ...search, label: "" });
		}
	};

	const setSearchAuthor = (value) => {
		if (value) {
			setSearch({ ...search, author: `&author__id__in=${value.map((x) => x.value).join(",")}` });
		} else {
			setSearch({ ...search, author: "" });
		}
	};

	const setSearchDate = (value, isStart) => {
		const s = { ...search, date: "" };
		if (isStart) {
			s.start = value;
		} else {
			s.end = value;
		}
		if (s.start) {
			s.date += `&date__gte=${s.start}`;
		}
		if (s.end) {
			s.date += `&date__lte=${s.end}`;
		}
		setSearch(s);
	};

	const setSorting = (value) => {
		["Title", "Type", "Date"].forEach((col) => {
			document.getElementById(col.toLowerCase()).innerHTML = col;
		});
		if (search.order.includes(`=${value}`)) {
			document.getElementById(value).innerHTML += " &uarr;";
			setSearch({ ...search, order: `&ordering=-${value}` });
		} else {
			document.getElementById(value).innerHTML += " &darr;";
			setSearch({ ...search, order: `&ordering=${value}` });
		}
	};

	const clearSelection = () => {
		const checkboxes = Array.from(document.querySelectorAll("input[type='checkbox']"));
		const checked = checkboxes.every((checkbox) => !checkbox.checked);
		checkboxes.forEach((checkbox) => {
			checkbox.checked = checked; // eslint-disable-line no-param-reassign
		});
	};

	useEffect(() => {
		if (timer.current) {
			clearTimeout(timer.current);
		}
		timer.current = setTimeout(() => {
			valSearch();
		}, 500);
	}, [search]);

	const getPublications = async () => {
		const response = await fetchBackend(`/api/v1/publication/?limit=${limit}`);
		const data = response.data;
		setCount(data.count);
		setPublications(data.results);
	};

	const getBibtex = () => {
		let selected = publications;
		const checkboxes = Array.from(document.querySelectorAll("input[type='checkbox']"));
		if (!checkboxes.every((checkbox) => !checkbox.checked)) {
			const selection = new Set();
			checkboxes.forEach((checkbox) => {
				if (checkbox.checked) selection.add(Number(checkbox.name));
			});
			selected = publications.filter((publication) => selection.has(publication.id));
		}
		const cites = selected.map((publication) => publication.cite).filter((cite) => cite);
		const bibtexs = selected.map((publication) => publication.bibtex);
		const text = `\\cite{${cites.join(",")}}\n\n${bibtexs.join("\n\n")}`;
		const file = new Blob([text], { type: "text/plain;charset=utf-8" });
		const element = document.createElement("a");
		element.href = URL.createObjectURL(file);
		element.download = "publications.bib";
		element.click();
	};

	useEffect(() => {
		getPublications();
		setLimit(100);
	}, []);

	useEffect(() => {
		window.MathJax.typeset();
	}, [publications]);

	const searchID = useId();
	return (
		<>
			<div className="grid grid-flow-col justify-between">
				<h1 className="font-bold leading-tight text-xl mb-5 ">Publications</h1>
				<div>
					<button onClick={getBibtex} type="button" className="btn btn-sm w-fit mb-5 text-white">
						Get Bibtex
					</button>
					&nbsp;&nbsp;&nbsp;
					<a href="/publication/add">
						<button type="button" className="btn btn-primary btn-sm w-fit mb-5 text-white">
							Add Publication
						</button>
					</a>
				</div>
			</div>
			<div className="publication_main overflow-x-auto pub-table w-auto ">
				<div>
					<div className="p-4 bg-gray-100 grid grid-flow-col gap-5 justify-between items-center min-w-fit rounded-t-lg shadow-md">
						<div className=" bg-gray-100 grid grid-flow-col gap-5 justify-between items-center ">
							<label htmlFor={searchID} className="sr-only">
								Search
							</label>
							<div className="relative">
								<div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
									<i className="fi fi-search text-gray-500" />
								</div>
								<form onSubmit={(e) => e.preventDefault()}>
									<input
										type="text"
										id={searchID}
										className="input w-full max-w-xs border-gray-300 focus:border-primary block pl-10"
										placeholder="Search for items"
										onChange={(e) => setSearch({ ...search, searchInput: e.target.value })}
									/>
								</form>
							</div>
							<div className="Select max-w-xs">
								<CustomSelect
									options={types.map((type) => ({ label: type.name, value: type.id }))}
									placeholder="Type"
									isMulti
									isClearable
									onChange={(value) => {
										setSearchType(value);
									}}
								/>
							</div>
							<div className="Select ">
								<CustomSelect
									options={labels.map((label) => ({ label: label.label, value: label.id }))}
									placeholder="Labels"
									isMulti
									isClearable
									onChange={(value) => {
										setSearchLabel(value);
									}}
								/>
							</div>
							<div className="Select">
								<CustomSelect
									options={authors.map((author) => ({ label: author.name, value: author.id }))}
									placeholder="Authors"
									isMulti
									isClearable
									onChange={(value) => {
										setSearchAuthor(value);
									}}
								/>
							</div>
							<div>From:</div>
							<div>
								<input
									type="Date"
									className="input input-bordered w-full max-w-s datepicker-input"
									isClearable
									onChange={(e) => {
										setSearchDate(e.target.value, true);
									}}
								/>
							</div>
							<div>To:</div>
							<div>
								<input
									type="Date"
									className="input input-bordered w-full max-w-s datepicker-input"
									isClearable
									onChange={(e) => {
										setSearchDate(e.target.value, false);
									}}
								/>
							</div>
						</div>
						<div className="text-primary">
							{(count > 1 || count === 0) && <div> {count} Publications found</div>}
							{count === 1 && <div> {count} Publication found</div>}
						</div>
					</div>
					<table className="w-full text-sm text-left text-gray-500 relative rounded-b-lg overflow-hidden shadow-md">
						<thead className="text-xs text-gray-700 uppercase bg-gray-100">
							<tr>
								<th
									scope="col"
									id="title"
									className="px-6 py-3"
									onClick={(e) => {
										setSorting(e.target.id);
									}}>
									Title
								</th>
								<th
									scope="col"
									id="type"
									className="px-6 py-3"
									onClick={(e) => {
										setSorting(e.target.id);
									}}>
									Type
								</th>
								<th
									scope="col"
									id="date"
									className="px-6 py-3"
									onClick={(e) => {
										setSorting(e.target.id);
									}}>
									Date &uarr;
								</th>
								<th scope="col" className="px-6 py-3">
									Authors
								</th>
								<th scope="col" className="px-6 py-3">
									Labels
								</th>
								<th scope="col" className="px-6 py-3" onClick={clearSelection}>
									Actions
									<span className="sr-only">Edit</span>
								</th>
							</tr>
						</thead>
						<tbody>
							{publications.map((publication) => (
								<tr key={publication.id} className="bg-white hover:bg-gray-50 h-10">
									<th
										scope="row"
										className="px-6 py-4 font-medium whitespace-nowrap max-w-xs truncate">
										<Link
											className="hover:text-primary transition ease-in-out"
											to={`/publication/${publication.id}`}>
											{publication.title}
										</Link>
									</th>
									<td className="px-6 py-4">{publication.type.name}</td>
									<td className="px-6 py-4">{publication.date}</td>
									<td className="px-6 py-4">
										{publication.author.map((author) => (
											<div key={author.id}>
												{author.name}, {author.firstname.charAt(0)}.
											</div>
										))}
									</td>
									<td>
										{publication.label.map((label) => (
											<div className="" key={label.id}>
												{label.label}
											</div>
										))}
									</td>
									<td>
										<div className="action px-6 py-4 grid grid-flow-col gap-1 justify-start">
											<button
												type="button"
												className="btn btn-sm max-w-fit btn-ghost bg-white p-1 text-gray-400 hover:text-white hover:bg-primary shadow-lg"
												title="Download PDF"
												disabled={!(publication.url || publication.file || publication.arxiv)}
												onClick={() => {
													window
														.open(
															publication.url ||
																publication.file ||
																`https://arxiv.org/pdf/${publication.arxiv}`,
															"_blank"
														)
														.focus();
												}}>
												<i className="action fi fi-download" />
											</button>
											<button
												type="button"
												className="btn btn-sm max-w-fit btn-ghost bg-white p-1 text-gray-400 hover:text-white hover:bg-amber-400 shadow-lg"
												title="Edit"
												onClick={() => {
													navigate(`publication/${publication.id}/edit/`);
												}}>
												<ion-icon name="pencil" />
											</button>
											<input type="checkbox" name={publication.id} />
										</div>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>
			</div>
		</>
	);
}

export default Publications;
