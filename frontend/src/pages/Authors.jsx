import React, { useState, useEffect } from "react";
import AuthorsForm from "./AuthorsForm";
import { fetchBackend } from "../logic/fetch-options";

function Authors() {
	const [authors, setAuthors] = useState([]);
	const [selectedAuthor, setSelectedAuthor] = useState("");
	const [user, setUser] = useState("");

	const getAuthors = async () => {
		const response = await fetchBackend("/api/v1/author/");
		setAuthors(response.data);
	};

	const getUser = async () => {
		const response = await fetchBackend("/whoami/");
		setUser(response.data.username);
	};

	useEffect(() => {
		getAuthors();
		getUser();
	}, []);
	return (
		<div>
			<h1 className="font-bold leading-tight text-xl mb-5">Authors</h1>
			<div className="grid lg:grid-flow-col gap-10 grid-flow-row">
				<div className="max-w-auto overflow-x-auto max-h-80vh shadow-md sm:rounded-lg h-fit w-auto">
					<table className="w-full text-sm text-left text-gray-500 relative">
						<thead className="text-xs text-gray-700 uppercase bg-gray-100 ">
							<tr>
								<th scope="col" className="px-6 py-3 ">
									Name
								</th>
								<th scope="col" className="px-6 py-3 ">
									First Name
								</th>
								<th scope="col" className="px-6 py-3 ">
									ORCID
								</th>
								<th scope="col" className="px-6 py-3 ">
									Actions
									<span className="sr-only">Edit</span>
								</th>
							</tr>
						</thead>
						<tbody>
							{authors.map((author) => (
								<tr key={author.id} className="bg-white hover:bg-gray-50">
									<th
										scope="row"
										className="px-6 py-4 font-medium text-gray-900 text-white whitespace-nowrap max-w-xs truncate">
										{author?.inspire && (
											<a
												href={`https://inspirehep.net/authors/${author.inspire}`}
												target="_blank"
												rel="noreferrer">
												{author.name}
											</a>
										)}
										{!author?.inspire && <div>{author.name}</div>}
									</th>
									<td className="px-6 py-4">{author.firstname}</td>
									<td className="px-6 py-4">{author.orcid}</td>
									<td className="px-6 py-4 grid gap-1 grid-flow-col justify-start">
										{user === "admin" && (
											<button
												type="button"
												className="btn btn-sm max-w-fit btn-ghost bg-white p-1 text-gray-400 hover:text-white hover:bg-amber-400 shadow-lg"
												title="Edit"
												onClick={() => {
													setSelectedAuthor({
														id: author.id,
														name: author.name,
														firstname: author.firstname,
														orcid: author.orcid,
														inspire: author.inspire
													});
												}}>
												<ion-icon name="pencil" />
											</button>
										)}
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>

				<AuthorsForm author={selectedAuthor} setAuthor={setSelectedAuthor} getAuthors={getAuthors} />
			</div>
		</div>
	);
}

export default Authors;
