import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import { fetchBackend } from "../logic/fetch-options";

function Publication() {
	const { id } = useParams();
	const navigate = useNavigate();
	const [publication, setPublication] = useState(null);
	const [disabledBTN, setDisabledBTN] = useState(null);

	const getPublication = async () => {
		const response = await fetchBackend(`/api/v1/publication/${id}/`);
		setPublication(response.data);
	};

	const preparePublication = () => {
		if (!(publication?.url || publication?.file || publication?.arxiv)) {
			setDisabledBTN("disabled");
		} else {
			setDisabledBTN();
		}
	};

	const journalRef = (pub) => {
		let result = "";
		result += pub?.number ? `, no.${pub?.number}` : "";
		result += pub?.pages ? `, ${pub?.pages}` : "";
		result += pub?.date ? ` (${pub?.date.slice(0, 4)})` : "";
		return result;
	};

	useEffect(() => {
		if (publication) {
			preparePublication();
		}
		window.MathJax.typeset();
	}, [publication]);

	useEffect(() => {
		getPublication();
	}, [id]);

	return (
		<>
			<div>
				<div className="leading-tight text-3xl mt-0 mb-2 font-bold">{publication?.title}</div>
			</div>
			<div className="lg:grid flex flex-col grid-cols-2 grid-flow-row gap-5 max-w-6xl">
				<div className="grid grid-flow-row gap-5 content-start">
					<div>
						<div className="font-bold">Release Date:</div>
						<div className="">{publication?.date}</div>
					</div>
					<div>
						<div className="font-bold">Publication Type:</div>
						<div className="">{publication?.type.name}</div>
					</div>
					<div>
						<div className="font-bold">Associated Authors:</div>

						<div className="flex gap-3 max-w-lg flex-wrap">
							{publication?.author.map((author) => (
								<div key={author.id} className="">
									{author.name}, {author.firstname.charAt(0)}.
								</div>
							))}
						</div>
					</div>
					<div>
						<div className="font-bold">Labels:</div>
						<div>
							{publication?.label.map((label) => (
								<div key={label.id}>{label.label}</div>
							))}
						</div>
					</div>
					{publication?.journal && (
						<div>
							<div className="font-bold">Journal:</div>
							<div className="">
								{publication?.journal?.brief} <b>{publication?.volume || ""}</b>
								{journalRef(publication)}
							</div>
						</div>
					)}
					{publication?.conference && (
						<div>
							<div className="font-bold">Conference:</div>
							<div className="">
								{publication?.conference.name} ({publication?.conference.brief})
							</div>
						</div>
					)}
					<div>
						<div className="font-bold">Links:</div>
						<div className="">
							{publication?.inspire && (
								<div>
									<a
										className="link text-primary"
										target="_blank"
										href={`https://inspirehep.net/literature/${publication?.inspire}`}
										rel="noreferrer">
										Inspire
									</a>
								</div>
							)}
							{publication?.arxiv && (
								<div>
									<a
										className="link text-primary"
										target="_blank"
										href={`https://arxiv.org/abs/${publication?.arxiv}`}
										rel="noreferrer">
										arXiv:{publication?.arxiv}
									</a>
								</div>
							)}
							{publication?.doi && (
								<div>
									<a
										className="link text-primary"
										target="_blank"
										href={`https://doi.org/${publication?.doi}`}
										rel="noreferrer">
										DOI: {publication?.doi}
									</a>
								</div>
							)}
						</div>
					</div>
					{publication?.comment && (
						<div>
							<div className="font-bold">Comment:</div>
							<div className="">{publication?.comment}</div>
						</div>
					)}
					<div className="grid grid-flow-col gap-4 justify-start">
						<button
							type="button"
							className="btn btn-md btn-ghost bg-white text-gray-400 hover:text-white hover:bg-primary shadow-lg tooltip tooltip-primary tooltip-top"
							data-tip="Download"
							onClick={() => {
								window
									.open(
										publication.url ||
											publication.file ||
											`https://arxiv.org/pdf/${publication.arxiv}`,
										"_blank"
									)
									.focus();
							}}
							disabled={disabledBTN}>
							<i className="action-pub fi fi-download" />
						</button>
						<button
							type="button"
							className="btn btn-md btn-ghost bg-white text-gray-400 text-xl hover:text-white hover:bg-secondary shadow-lg tooltip tooltip-secondary tooltip-top"
							data-tip="Copy BibTex"
							onClick={() => {
								navigator.clipboard.writeText(publication?.bibtex);
								toast.success("Bibtex has been copied to clipboard", { theme: "colored" });
							}}>
							<ion-icon name="copy" />
						</button>
						<button
							type="button"
							className="btn btn-md max-w-fit btn-ghost bg-white text-gray-400 hover:text-white hover:bg-amber-400 shadow-lg tooltip tooltip-warning tooltip-top"
							data-tip="Edit"
							onClick={() => {
								navigate(`./edit/`);
							}}>
							<ion-icon name="pencil" size="large" />
						</button>
					</div>
				</div>
				<div className="">
					<div className="font-bold">Bibtex:</div>
					<div className="bg-gray-200 rounded-lg p-2 text-xs max-w-xs overflow-x-auto">
						<div className="text-sm bibtex leading-4 whitespace-pre">{publication?.bibtex}</div>
					</div>
				</div>
			</div>
		</>
	);
}

export default Publication;
