import React, { useId } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { fetchBackend } from "../logic/fetch-options";
import { importAuthor } from "../logic/import-inspire";

function AuthorsForm({ author, setAuthor, getAuthors }) {
	const createAuthor = async () => {
		fetchBackend(`/api/v1/author_post/`, "POST", author)
			.then(() => {
				getAuthors();
				toast.success("The author has been created!", { theme: "colored" });
				setAuthor({ name: "", firstname: "", inspire: "", orcid: "" });
			})
			.catch((error) => {
				toast.error("Author creation failed", { theme: "colored" });
				console.log(error);
			});
	};
	const updateAuthor = async () => {
		fetchBackend(`/api/v1/author_post/${author.id}/`, "PUT", author)
			.then(() => {
				getAuthors();
				toast.success("The author has been updated!", { theme: "colored" });
				setAuthor({ name: "", firstname: "", inspire: "", orcid: "", id: "" });
			})
			.catch((error) => {
				toast.error("Author update failed", { theme: "colored" });
				console.log(error);
			});
	};
	const nameInput = useId();
	const firstnameInput = useId();
	const orcidInput = useId();
	const inspireInput = useId();

	const callImportInspire = () => {
		importAuthor(author, setAuthor);
	};

	const valInput = () => {
		if (author.name && author.firstname) {
			if (author.id) {
				updateAuthor();
			} else {
				createAuthor();
			}
		} else {
			toast.error("Fill out all necessary Fields", { theme: "colored" });
		}
	};

	return (
		<div>
			<form className="flex flex-col flex-wrap gap-5 max-w-xs" onSubmit={(e) => e.preventDefault()}>
				<div>
					<label htmlFor={nameInput} className="text-sm mr-4 font-semibold">
						Name*
					</label>
					<input
						type="text"
						id={nameInput}
						className="input input-bordered w-full max-w-xs"
						value={author?.name ?? ""}
						onChange={(e) => {
							setAuthor({ ...author, name: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={firstnameInput} className="text-sm mr-4 font-semibold">
						First Name*
					</label>
					<input
						type="text"
						id={firstnameInput}
						value={author?.firstname ?? ""}
						className="input input-bordered w-full max-w-xs"
						onChange={(e) => {
							setAuthor({ ...author, firstname: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={orcidInput} className="text-sm mr-4 font-semibold">
						ORCID
					</label>
					<input
						type="text"
						id={orcidInput}
						value={author?.orcid ?? ""}
						className="input input-bordered w-full max-w-s "
						onChange={(e) => {
							setAuthor({ ...author, orcid: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={inspireInput} className="text-sm mr-4 font-semibold">
						inspire ID
					</label>
					<input
						type="text"
						id={inspireInput}
						value={author?.inspire ?? ""}
						className="input input-bordered w-full max-w-s "
						onChange={(e) => {
							setAuthor({ ...author, inspire: e.target.value });
						}}
					/>
				</div>
				<button onClick={callImportInspire} type="button" className="btn btn-sm text-white grid-col">
					Import from inspire
				</button>
				<button onClick={valInput} type="submit" className="btn btn-sm btn-primary text-white">
					Submit
				</button>
			</form>
		</div>
	);
}

export default AuthorsForm;
