import React, { useId, useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import CustomSelect from "../components/CustomSelect";
import { fetchBackend } from "../logic/fetch-options";
import { getMasterData } from "../logic/get-masterdata";
import { generateBibtex } from "../logic/bibtex-generator";
import { importPublication } from "../logic/import-inspire";

function Create() {
	const { id } = useParams();
	const [publication, setPublication] = useState({});
	const navigate = useNavigate();
	const { types, labels, collaborations, authors, conferences, journals } = getMasterData();
	const [headline, setHeadline] = useState("");
	const [file, setFile] = useState("");

	const cleanPublication = (data) => {
		const x = data;
		if (!x.url) {
			delete x.url;
			setPublication(x);
		}
		setPublication(x);
	};

	const getPublication = async () => {
		const response = await fetchBackend(`/api/v1/publication/${id}/`);
		cleanPublication(response.data);
	};

	const prepPublication = () => {
		const pub = structuredClone(publication);
		delete pub.author;
		if (publication.author) {
			pub.author = publication.author.map(({ id }) => id);
		}
		delete pub.presenter;
		if (publication.presenter) {
			pub.presenter = publication.presenter.id;
		}
		delete pub.label;
		if (publication.label) {
			pub.label = publication.label.map(({ id }) => id);
		}
		delete pub.type;
		if (publication.type) {
			pub.type = publication.type.id;
		}
		delete pub.collaboration;
		if (publication.collaboration) {
			pub.collaboration = publication.collaboration.id;
		}
		delete pub.journal;
		if (publication.journal) {
			pub.journal = publication.journal.id;
		}
		delete pub.conference;
		if (publication.conference) {
			pub.conference = publication.conference.id;
		}
		delete pub.file;
		return pub;
	};

	const createPublication = async () => {
		const pub = prepPublication();
		fetchBackend(`/api/v1/pub_post/`, "POST", pub)
			.then(() => {
				toast.success("Publication created successfully", { theme: "colored" });
				navigate("/");
			})
			.catch((error) => {
				toast.error("Publication creation failed", { theme: "colored" });
				console.log(error);
			});
	};
	const updatePublication = async () => {
		const pub = prepPublication();
		fetchBackend(`/api/v1/pub_post/${id}/`, "PUT", pub)
			.then(() => {
				toast.success("Publication updated successfully", { theme: "colored" });
				navigate("/");
			})
			.catch((error) => {
				toast.error("Publication update failed", { theme: "colored" });
				console.log(error);
			});
	};

	const valPublication = () => {
		if (
			!publication.title ||
			publication.title === "" ||
			!publication.type ||
			!publication.author ||
			publication.author.length === 0 ||
			(["Talk", "Poster", "Proceedings"].includes(publication.type.name) &&
				(!publication.presenter || !publication.conference))
		) {
			toast.error("Fill out all necessary Fields", { theme: "colored" });
			return;
		}
		if (!publication.bibtex || publication.bibtex === "") {
			const bibtex = generateBibtex(publication);
			publication.bibtex = bibtex;
		}
		if (!publication.cite) {
			const match = publication.bibtex.match(/@\w*{\s*(.*)\s*,/);
			if (match) {
				publication.cite = match[1];
			}
		}
		if (id) {
			updatePublication();
		} else {
			createPublication();
		}
		if (file) {
			fetchBackend(`/api/v1/upload/${id}/${file.name}`, "PUT", {}, file);
		}
	};

	const typeInput = useId();
	const labelsInput = useId();
	const titleInput = useId();
	const titleTextInput = useId();
	const titleGermanInput = useId();
	const titleGermanTextInput = useId();
	const dateInput = useId();
	const collaborationInput = useId();
	const authorsInput = useId();
	const authorListInput = useId();
	const presenterInput = useId();
	const conferenceInput = useId();
	const journalInput = useId();
	const volumeInput = useId();
	const numberInput = useId();
	const pagesInput = useId();
	const doiInput = useId();
	const arxivInput = useId();
	const inspireInput = useId();
	const bibtexInput = useId();
	const citeInput = useId();
	const urlInput = useId();
	const fileInput = useId();
	const commentInput = useId();

	const checkId = () => {
		if (id) {
			setHeadline("Update");
			getPublication();
		} else {
			setHeadline("Add");
		}
	};

	const genBibtex = () => {
		setPublication({ ...publication, bibtex: generateBibtex(publication) });
	};
	const callImportInspire = () => {
		importPublication(publication, setPublication, journals, collaborations, authors);
	};

	useEffect(() => {
		checkId();
	}, []);
	return (
		<div className="">
			<h1 className="font-bold leading-tight text-xl mb-8">{headline} Publication</h1>
			<form className="lg:grid flex flex-col grid-cols-2 gap-10  min-h-96 max-w-6xl">
				<div className="sm:grid flex flex-col grid-cols-2 gap-5">
					<div>
						<label htmlFor={typeInput} className="text-sm mr-4 font-semibold block mb-2">
							Type*
						</label>
						<CustomSelect
							options={types.map((type) => ({ label: type.name, value: type.id }))}
							placeholder="Select Type"
							onChange={(value) => {
								setPublication({ ...publication, type: types.find((x) => x.id === value.value) });
							}}
							value={publication?.type?.id}
						/>
					</div>
					<div>
						<label htmlFor={labelsInput} className="text-sm mr-4 font-semibold block mb-2">
							Labels
						</label>
						<CustomSelect
							options={labels.map((label) => ({ label: label.label, value: label.id }))}
							placeholder="Select Labels"
							isMulti
							id={labelsInput}
							onChange={(value) => {
								setPublication({
									...publication,
									label: value.map((label) => labels.find((x) => x.id === label.value))
								});
							}}
							value={publication?.label?.map((l) => l.id)}
						/>
					</div>
					<div>
						<label htmlFor={titleInput} className="text-sm mr-4 font-semibold block mb-2">
							Title (latex format)*
						</label>
						<input
							type="text"
							id={titleInput}
							className="input input-bordered w-full max-w-s"
							value={publication?.title ?? ""}
							onChange={(e) => {
								setPublication({ ...publication, title: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={titleTextInput} className="text-sm mr-4 font-semibold block mb-2">
							Title (text format)
						</label>
						<input
							type="text"
							id={titleTextInput}
							className="input input-bordered w-full max-w-s"
							value={publication?.title_text ?? ""}
							onChange={(e) => {
								setPublication({ ...publication, title_text: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={titleGermanInput} className="text-sm mr-4 font-semibold block mb-2">
							German Title (latex format)
						</label>
						<input
							type="text"
							id={titleGermanInput}
							className="input input-bordered w-full max-w-s"
							value={publication?.title_german ?? ""}
							onChange={(e) => {
								setPublication({ ...publication, title_german: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={titleGermanTextInput} className="text-sm mr-4 font-semibold block mb-2">
							German Title (text format)
						</label>
						<input
							type="text"
							id={titleGermanTextInput}
							className="input input-bordered w-full max-w-s"
							value={publication?.title_german_text ?? ""}
							onChange={(e) => {
								setPublication({ ...publication, title_german_text: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={dateInput} className="text-sm mr-4 font-semibold block mb-2">
							Date*
						</label>
						<DatePicker
							id={dateInput}
							selected={publication?.date ?? ""}
							dateFormat="yyyy/MM/dd"
							placeholderText="yyyy/mm/dd"
							className="input input-bordered w-full max-w-s datepicker-input"
							onChange={(selectedDate) => {
								setPublication({ ...publication, date: selectedDate.toISOString().split("T")[0] });
							}}
						/>
					</div>
					<div>
						<label htmlFor={collaborationInput} className="text-sm mr-4 font-semibold block mb-2">
							Collaboration
						</label>
						<CustomSelect
							id={collaborationInput}
							options={collaborations.map((collaboration) => ({
								label: collaboration.name,
								value: collaboration.id
							}))}
							placeholder="Select Collaboration"
							isClearable
							onChange={(value) => {
								if (value) {
									setPublication({
										...publication,
										collaboration: collaborations.find((x) => x.id === value.value)
									});
								} else {
									setPublication({ ...publication, collaboration: null });
								}
							}}
							value={publication?.collaboration?.id}
						/>
					</div>
					<div>
						<label htmlFor={authorsInput} className="text-sm mr-4 font-semibold block mb-2 ">
							Authors (from AG Kuhr)*
						</label>
						<CustomSelect
							options={authors.map((author) => ({
								label: `${author.name}, ${author.firstname}`,
								value: author.id
							}))}
							placeholder="Select Author"
							className="min-w-max"
							isMulti
							id={authorsInput}
							onChange={(value) => {
								setPublication({
									...publication,
									author: value.map((author) => authors.find((x) => x.id === author.value))
								});
							}}
							value={publication?.author?.map((a) => a.id)}
						/>
					</div>
					<div>
						<label htmlFor={authorListInput} className="text-sm mr-4 font-semibold block mb-2">
							List of all authors in bibtex format
						</label>
						<input
							type="text"
							id={authorListInput}
							value={publication?.author_list ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, author_list: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={presenterInput} className="text-sm mr-4 font-semibold block mb-2">
							Presenter
						</label>
						<CustomSelect
							options={authors.map((author) => ({
								label: `${author.name}, ${author.firstname}`,
								value: author.id
							}))}
							className="min-w-max"
							id={presenterInput}
							onChange={(value) => {
								setPublication({
									...publication,
									presenter: authors.find((x) => x.id === value.value)
								});
							}}
							value={publication?.presenter?.id}
						/>
					</div>
					<div>
						<label htmlFor={conferenceInput} className="text-sm mr-4 font-semibold block mb-2">
							Conference
						</label>
						<CustomSelect
							id={conferenceInput}
							options={conferences.map((conference) => ({
								label: conference.brief,
								value: conference.id
							}))}
							placeholder="Select Conference"
							isClearable
							onChange={(value) => {
								if (value) {
									setPublication({
										...publication,
										conference: conferences.find((x) => x.id === value.value)
									});
								} else {
									setPublication({ ...publication, conference: null });
								}
							}}
							value={publication?.conference?.id}
						/>
					</div>
					<div>
						<label htmlFor={journalInput} className="text-sm mr-4 font-semibold block mb-2">
							Journal
						</label>
						<CustomSelect
							id={journalInput}
							options={journals.map((journal) => ({ label: journal.brief, value: journal.id }))}
							placeholder="Select Journal"
							isClearable
							onChange={(value) => {
								if (value) {
									setPublication({
										...publication,
										journal: journals.find((x) => x.id === value.value)
									});
								} else {
									setPublication({ ...publication, journal: null });
								}
							}}
							value={publication?.journal?.id}
						/>
					</div>
					<div>
						<label htmlFor={volumeInput} className="text-sm mr-4 font-semibold block mb-2">
							Volume
						</label>
						<input
							type="text"
							id={volumeInput}
							value={publication?.volume ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, volume: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={numberInput} className="text-sm mr-4 font-semibold block mb-2">
							Number
						</label>
						<input
							type="text"
							id={numberInput}
							value={publication?.number ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, number: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={pagesInput} className="text-sm mr-4 font-semibold block mb-2">
							Pages
						</label>
						<input
							type="text"
							id={pagesInput}
							value={publication?.pages ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, pages: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={doiInput} className="text-sm mr-4 font-semibold block mb-2">
							DOI
						</label>
						<input
							type="text"
							id={doiInput}
							value={publication?.doi ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, doi: e.target.value });
							}}
						/>
					</div>
					<div />
					<div>
						<label htmlFor={arxivInput} className="text-sm mr-4 font-semibold block mb-2">
							arXiv ID
						</label>
						<input
							type="text"
							id={arxivInput}
							value={publication?.arxiv ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, arxiv: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={inspireInput} className="text-sm mr-4 font-semibold block mb-2">
							inspire ID
						</label>
						<input
							type="text"
							id={inspireInput}
							value={publication?.inspire ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, inspire: e.target.value });
							}}
						/>
					</div>
				</div>
				<div className="grid items-baseline w-full">
					<div>
						<label htmlFor={citeInput} className="text-sm mr-4 font-semibold block mb-2">
							Citation key
						</label>
						<input
							type="text"
							id={citeInput}
							value={publication?.cite ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, cite: e.target.value });
							}}
						/>
					</div>
					<div className="min-w-full">
						<label htmlFor={bibtexInput} className="text-sm mr-4 font-semibold block mb-2">
							Bibtex
						</label>
						<textarea
							id={bibtexInput}
							value={publication?.bibtex ?? ""}
							rows={15}
							onChange={(e) => {
								setPublication({ ...publication, bibtex: e.target.value });
							}}
							style={{ resize: "none" }}
							className="code leading-4 textarea textarea-bordered min-w-full"
						/>
					</div>
					<div>
						<button onClick={genBibtex} type="button" className="btn btn-sm text-white grid-col">
							Generate/update bibtex
						</button>
					</div>
					<div>
						<button onClick={callImportInspire} type="button" className="btn btn-sm text-white grid-col">
							Import from inspire
						</button>
					</div>
					<div>
						<label htmlFor={urlInput} className="text-sm mr-4 font-semibold block mb-2">
							URL
						</label>
						<input
							type="url"
							id={urlInput}
							value={publication?.url ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, url: e.target.value });
							}}
						/>
					</div>
					<div>
						<label htmlFor={fileInput}>PDF File: </label>
						<input
							type="file"
							id={fileInput}
							onChange={(e) => {
								if (e.target.files) setFile(e.target.files[0]);
							}}
						/>
					</div>
					<div>
						<label htmlFor={commentInput} className="text-sm mr-4 font-semibold block mb-2">
							Comment
						</label>
						<input
							type="text"
							id={commentInput}
							value={publication?.comment ?? ""}
							className="input input-bordered w-full max-w-s "
							onChange={(e) => {
								setPublication({ ...publication, comment: e.target.value });
							}}
						/>
					</div>
				</div>
				<button
					onClick={valPublication}
					type="button"
					className="btn btn-sm btn-primary text-white grid-col"
					style={{ gridColumn: "2 span" }}>
					Submit
				</button>
			</form>
		</div>
	);
}

export default Create;
