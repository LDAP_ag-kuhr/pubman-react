import React, { useId } from "react";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { fetchBackend } from "../logic/fetch-options";
import { importConference } from "../logic/import-inspire";

function ConferencesForm({ conference, setConference, getConferences }) {
	const createConference = async () => {
		fetchBackend(`/api/v1/conference/`, "POST", conference)
			.then(() => {
				getConferences();
				toast.success("The conference has been created!", { theme: "colored" });
				setConference({ brief: "", name: "", name_german: "", location: "", url: "", start: "", end: "" });
			})
			.catch((error) => {
				toast.error("Conference creation failed", { theme: "colored" });
				console.log(error);
			});
	};
	const updateConference = async () => {
		fetchBackend(`/api/v1/conference/${conference.id}/`, "PUT", conference)
			.then(() => {
				getConferences();
				toast.success("The conference has been updated!", { theme: "colored" });
				setConference({
					brief: "",
					name: "",
					name_german: "",
					location: "",
					url: "",
					start: "",
					end: "",
					id: ""
				});
			})
			.catch((error) => {
				toast.error("Conference update failed", { theme: "colored" });
				console.log(error);
			});
	};
	const briefInput = useId();
	const nameInput = useId();
	const nameGermanInput = useId();
	const urlInput = useId();
	const locationInput = useId();
	const startInput = useId();
	const endInput = useId();
	const inspireInput = useId();

	const callImportInspire = () => {
		importConference(conference, setConference);
	};

	const valInput = () => {
		if (conference.brief && conference.name && conference.start && conference.end) {
			if (conference.id) {
				updateConference();
			} else {
				createConference();
			}
		} else {
			toast.error("Fill out all necessary Fields", { theme: "colored" });
		}
	};

	return (
		<div>
			<form className="flex flex-col flex-wrap gap-5 max-w-xs" onSubmit={(e) => e.preventDefault()}>
				<div>
					<label htmlFor={briefInput} className="text-sm mr-4 font-semibold">
						Brief Name*
					</label>
					<input
						type="text"
						id={briefInput}
						className="input input-bordered w-full max-w-xs"
						value={conference?.brief ?? ""}
						onChange={(e) => {
							setConference({ ...conference, brief: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={nameInput} className="text-sm mr-4 font-semibold">
						Full Name*
					</label>
					<input
						type="text"
						id={nameInput}
						value={conference?.name ?? ""}
						className="input input-bordered w-full max-w-xs"
						onChange={(e) => {
							setConference({ ...conference, name: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={nameGermanInput} className="text-sm mr-4 font-semibold">
						German Name
					</label>
					<input
						type="text"
						id={nameGermanInput}
						value={conference?.name_german ?? ""}
						className="input input-bordered w-full max-w-xs"
						onChange={(e) => {
							setConference({ ...conference, name_german: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={locationInput} className="text-sm mr-4 font-semibold">
						Location
					</label>
					<input
						type="text"
						id={locationInput}
						value={conference?.location ?? ""}
						className="input input-bordered w-full max-w-xs"
						onChange={(e) => {
							setConference({ ...conference, location: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={urlInput} className="text-sm mr-4 font-semibold">
						URL
					</label>
					<input
						type="url"
						id={urlInput}
						value={conference?.url ?? ""}
						className="input input-bordered w-full max-w-s "
						onChange={(e) => {
							setConference({ ...conference, url: e.target.value });
						}}
					/>
				</div>
				<div>
					<label htmlFor={startInput} className="text-sm mr-4 font-semibold">
						Start Date*
					</label>
					<DatePicker
						id={startInput}
						selected={conference?.start ?? null}
						dateFormat="yyyy/MM/dd"
						placeholderText="yyyy/mm/dd"
						className="input input-bordered w-full max-w-xs datepicker-input"
						onChange={(selectedDate) => {
							setConference({ ...conference, start: selectedDate.toISOString().split("T")[0] });
						}}
					/>
				</div>
				<div>
					<label htmlFor={endInput} className="text-sm mr-4 font-semibold">
						End Date*
					</label>
					<DatePicker
						id={endInput}
						selected={conference?.end ?? null}
						dateFormat="yyyy/MM/dd"
						placeholderText="yyyy/mm/dd"
						className="input input-bordered w-full max-w-xs datepicker-input"
						onChange={(selectedDate) => {
							setConference({ ...conference, end: selectedDate.toISOString().split("T")[0] });
						}}
					/>
				</div>
				<div>
					<label htmlFor={inspireInput} className="text-sm mr-4 font-semibold">
						inspire ID
					</label>
					<input
						type="text"
						id={inspireInput}
						value={conference?.inspire ?? ""}
						className="input input-bordered w-full max-w-s "
						onChange={(e) => {
							setConference({ ...conference, inspire: e.target.value });
						}}
					/>
				</div>
				<button onClick={callImportInspire} type="button" className="btn btn-sm text-white grid-col">
					Import from inspire
				</button>
				<button onClick={valInput} type="submit" className="btn btn-sm btn-primary text-white">
					Submit
				</button>
			</form>
		</div>
	);
}

export default ConferencesForm;
