import React, { useState, useEffect } from "react";
import ConferencesForm from "./ConferencesForm";
import { fetchBackend } from "../logic/fetch-options";

function Conferences() {
	const [conferences, setConferences] = useState([]);
	const [selectedConference, setSelectedConference] = useState("");

	const getConferences = async () => {
		const response = await fetchBackend("/api/v1/conference/");
		setConferences(response.data);
	};

	useEffect(() => {
		getConferences();
	}, []);

	useEffect(() => {
		window.MathJax.typeset();
	}, [conferences]);

	return (
		<div>
			<h1 className="font-bold leading-tight text-xl mb-5">Conferences</h1>
			<div className="grid lg:grid-flow-col gap-10 grid-flow-row">
				<div className="max-w-auto overflow-x-auto max-h-80vh shadow-md sm:rounded-lg h-fit w-auto">
					<table className="w-full text-sm text-left text-gray-500 relative">
						<thead className="text-xs text-gray-700 uppercase bg-gray-100 ">
							<tr>
								<th scope="col" className="px-6 py-3 ">
									Brief name
								</th>
								<th scope="col" className="px-6 py-3 ">
									Full name
								</th>
								<th scope="col" className="px-6 py-3 ">
									Location
								</th>
								<th scope="col" className="px-6 py-3 ">
									Date
								</th>
								<th scope="col" className="px-6 py-3 ">
									Actions
									<span className="sr-only">Edit</span>
								</th>
							</tr>
						</thead>
						<tbody>
							{conferences.map((conference) => (
								<tr key={conference.id} className="bg-white hover:bg-gray-50">
									<th
										scope="row"
										className="px-6 py-4 font-medium text-gray-900 text-white whitespace-nowrap max-w-xs truncate">
										{conference?.url && (
											<a href={conference?.url} target="_blank" rel="noreferrer">
												{conference.brief}
											</a>
										)}
										{!conference?.url && <div>{conference.brief}</div>}
									</th>
									<td className="px-6 py-4">{conference.name}</td>
									<td className="px-6 py-4">{conference?.location}</td>
									<td className="px-6 py-4">
										{conference?.start} - {conference?.end}
									</td>
									<td className="px-6 py-4 grid gap-1 grid-flow-col justify-start">
										<button
											type="button"
											className="btn btn-sm max-w-fit btn-ghost bg-white p-1 text-gray-400 hover:text-white hover:bg-amber-400 shadow-lg"
											title="Edit"
											onClick={() => {
												setSelectedConference({
													id: conference.id,
													brief: conference.brief,
													name: conference.name,
													name_german: conference.name_german,
													url: conference.url,
													location: conference.location,
													start: conference.start,
													end: conference.end,
													inspire: conference.inspire
												});
											}}>
											<ion-icon name="pencil" />
										</button>
									</td>
								</tr>
							))}
						</tbody>
					</table>
				</div>

				<ConferencesForm
					conference={selectedConference}
					setConference={setSelectedConference}
					getConferences={getConferences}
				/>
			</div>
		</div>
	);
}

export default Conferences;
