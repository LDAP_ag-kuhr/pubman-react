<img src="frontend/public/static/logo192.png" width="100px" />

# Pubman

Pubman is a web application that allows you to store, access, and filter publications.
It is possible to upload different publication types like journal publications, talks, and theses.
You can save a file link to the publication and easily access bibtex code.

The Pubman installation of AG Kuhr is running on gar-cn-etp01.
You can connect to it from within the local network by going to http://gar-cn-etp01:8000.
From outside you can make an ssh tunnel with `ssh -L 8000:10.153.232.4:8000 -J USERNAME@z-sv-login01.server.physik.uni-muenchen.de USERNAME@gar-cn-etp01.garching.physik.uni-muenchen.de` where `USERNAME` is your user name and then connect to http://localhost:8000.

Bugs can be reported at https://gitlab.physik.uni-muenchen.de/LDAP_ag-kuhr/pubman-react/-/issues

## Main Page

On the main page publications can be viewed, searched, and filtered.
There are buttons for downloading the related file, and editing the publication.
At the top there is also a button to get the bibtex of the selected publications and a button to add a new publication.

![](doc/main.png)

## Create Publication

On the create publication page you can add a new publication to the system by selecting a type, adding a title, setting a date, and selecting the authors from AG Kuhr.
If the title contains latex code, a title version for output in plain text format can be provided in addition.
If a German title appears on the document it should be given in addition to the English one.

One or more labels should be selected to categorize the document.
This is used, for example, to attribute a document to a certain project or funding source.
If you have questions regarding the labels or would like to add a label contact [Thomas](mailto:Thomas.Kuhr@lmu.de).

If the document is authored by a collaboration then select this.
Under Authors select those who contributed as members of the AG Kuhr.
If an AG Kuhr author is not yet in the system then add him or her as explained below.
The full list of authors, as it will appear in the bibtex, is entered in the next field.

For talks, posters, and proceedings a presenter and a conference should be selected.
If a conference is not yet in the system then add it as explained below.
Do not add presentations at internal meetings, such as Belle II meetings or BJAMs.

For journal publications select the journal.
If the journal is not yet in the system than ask [Thomas](mailto:Thomas.Kuhr@lmu.de) to add it.
Volume, number, pages, and number are used for the generation of a bibtex entry.
The identifier of the bibtex entry is given in the Citation key field.
Use the `Generate/Update Bibtex` button to fill the Bibtex field.
You can also edit the bibtex manually.

DOI, arXiv ID, and Inspire ID are identifiers of the document and used to create links to the document in the respective repositories.
In addition a URL can be provided, e.g. to a talk on an indico page.
If the document is not available online it can be uploaded.

Finally an arbitrary comment can be added.

**Important**: If an inspire entry exists for the document one can just enter the inspire ID (the number in the inspirehep.net URL) and then click `Import from Inspire` to automatically get most of the metadata.

![](doc/publication.png)

## List / Create AG Kuhr Authors

On the authors page you can see a list of current and former AG Kuhr members.
New authors can be added.
Before adding yourself or somebody else make sure that no such entry exists already.
Name and ORCID can be obtained automatically from Inspire if an Inspire ID is given.
If an entry should be updated (e.g. to add an ORCID or Inspire ID) ask [Thomas](mailto:Thomas.Kuhr@lmu.de).

![](doc/author.png)

## List / Create Conferences

On the conferences page you can see a list of conferences that AG Kuhr members attended.
Existing entries can be edited and new ones can be added.
Again, be careful not to create duplicate entries for the same conference.
Follow conventions for the brief name.
If an Inspire entry exists metadata can be imported from there.

![](doc/conference.png)



## API
Pubman has a detailed documentation for the API at: https://gar-cn-etp01:8000/api/docs/.
Once authentificated, you can see all endpoints, make test requests and easily access python/shell/javascript code to retrieve data from the endpoints.

## Installation

1. Clone repository

```
git clone git@gitlab.physik.uni-muenchen.de:LDAP_ag-kuhr/pubman-react.git
cd pubman-react
```

2. Build frontend

The react frontend is built with Node.js:
```
cd frontend
npm run build
```

3. Create link to frontend

```
cd ../react_django
ln -s ../frontend .
```

4. Set up virtual environment

```
python -m venv venv
source venv/bin/activate
pip install -r ../requirements.txt
```

5. Run Pubman

```
python ./manage.py runserver localhost:8000
```
