#!/usr/bin/env python3

import argparse
import requests
import datetime
import json


headers = {'Accept': 'application/json'}


def date(str):
    return datetime.datetime.strptime(str, '%Y-%m-%d').date()


def get_argument_parser():
    parser = argparse.ArgumentParser(description='Get list of publications from PubMan')

    parser.add_argument('-o', '--output', default='bibtex', help='Output format')
    parser.add_argument('-s', '--server', default='127.0.0.1:8000', help='URL of the PubMan server')
    parser.add_argument('-t', '--type', action='append', choices=['Publication', 'Preprint', 'PhD Thesis', 'Master Thesis', 'Bachelor Thesis', 'Talk', 'Poster', 'Proceedings', 'Software'], help='Type of publication')
    parser.add_argument('-l', '--label', action='append', help='Publication label')
    parser.add_argument('-a', '--author', action='append', help='AG Kuhr author, either lastname or firstname.lastname')
    parser.add_argument('-f', '--from', type=date, dest='fromdate', help='From date in format Y-m-d')
    parser.add_argument('-u', '--until', type=date, dest='untildate', help='Until date in format Y-m-d')
    parser.add_argument('-y', '--year', type=int, help='Year')

    return parser


def get_types(server):
    return requests.get(f'http://{server}/api/v1/type/', headers=headers).json()


def get_labels(server):
    return requests.get(f'http://{server}/api/v1/label/', headers=headers).json()


def get_authors(server):
    return requests.get(f'http://{server}/api/v1/author/', headers=headers).json()


def get_publications(args):
    server = args.server
    
    params = {}
    if args.type:
        types = get_types(server)
        params['type__id__in'] = [type['id'] for type in types if type['name'] in args.type]
    if args.label:
        labels = get_labels(server)
        params['label__id__in'] = [label['id'] for label in labels if label['label'] in args.label]
    if args.author:
        authors = get_authors(server)
        print(authors)
        params['author__id__in'] = [author['id'] for author in authors if (author['name'] in args.author or f'{author["firstname"]}.{author["name"]}' in args.author)]
    if args.fromdate:
        params['date__gte'] = args.fromdate
    if args.untildate:
        params['date__lte'] = args.untildate
    if args.year:
        params['date__range'] = str(datetime.date(args.year, 1, 1)) + ',' + str(datetime.date(args.year + 1, 1, 1))

    result = requests.get(f'http://{server}/api/v1/publication/', headers=headers, params=params)

    return result.json()


def print_bibtex(publications):
    for publication in publications:
        if 'bibtex' in publication.keys():
            print(publication['bibtex'])
            print()


def author_list(publication):
    if publication['author_list']:
        return publication['author_list']
    else:
        return ', '.join([f"{author['firstname'][0]}.~{author['name']}" for author in publication['author']])


def reference(publication):
    if publication['journal']:
        return f"{publication['journal']['brief']} {publication['volume']}, {publication['number']}, {publication['pages']}"
    elif publication['arxiv']:
        return f"arXiv:{publication['arxiv']}"
    elif publication['doi']:
        return f"https://doi.org/{publication['doi']}"
    elif publication['type']['name'] == 'Proceedings' and publication['conference']:
        return f"Proceedings of {publication['conference']['name']}"
    else:
        return ""


def print_bibitem(publications):
    for publication in sorted(publications, key=lambda pub: pub['date']):
        type = publication['type']['name']
        authors = [f"{author['firstname'][0]}.~{author['name']}" for author in publication['author']]
        year = publication['date'][:4]
        if type in ['Publication', 'Proceedings']:
            print(f"""\\bibitem{{{publication['cite']}}}
  {author_list(publication)},
  \\textit{{{publication['title']}}},
  {reference(publication)} ({year}).
""")
        elif type == 'Preprint':
            print(f"""\\bibitem{{{publication['cite']}}}
  {author_list(publication)},
  \\textit{{{publication['title']}}},
  {reference(publication)} ({year}).
""")
        elif type in ['PhD Thesis', 'Master Thesis', 'Bachelor Thesis']:
            type = type.split()[0].replace('PhD', 'Doktor')
            print(f"""\\bibitem{{{type}{publication['author'][0]['name']}}}
  {authors[0]},
  \\textit{{{publication['title']}}},
  LMU, {type}arbeit ({year}).
""")
        elif type in ['Talk', 'Poster']:
            print(f"""\\bibitem{{{publication['cite']}}}
  {author_list(publication)},
  \\textit{{{publication['title']}}},
  {publication['conference']['brief']}, {publication['conference']['location']} ({year}).
""")
        elif type == 'Software':
            print(f"""\\bibitem{{{publication['cite']}}}
  {author_list(publication)},
  \\textit{{{publication['title']}}},
  {reference(publication)}.
""")


if __name__ == "__main__":
    parser = get_argument_parser()
    args = parser.parse_args()

    publications = get_publications(args)
    if args.output == 'json':
        print(json.dumps(publications, indent=4))
    elif args.output == 'bibtex':
        print_bibtex(publications)
    elif args.output == 'bibitem':
        print_bibitem(publications)