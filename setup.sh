#!/bin/bash

DIR=$(realpath $(dirname "$0"))
cd ${DIR}

python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt

django-admin startproject pubman .
cd pubman
patch -p0 < ../settings.patch
rm urls.py
ln -s ../urls.py .

cd ${DIR}
./manage.py makemigrations api admin
./manage.py migrate
echo "Please enter the data to create an admin user account:"
./manage.py createsuperuser

docker run --rm -v ${DIR}:/pubman -w /pubman/frontend node npm install
docker run --rm -v ${DIR}:/pubman -w /pubman/frontend node npm run build
